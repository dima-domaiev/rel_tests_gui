@EditorGeneral
Feature: Editor General

  @REL-840
  Scenario: General Editor SRS
    #specific features should be selected each When
    #background
    Given I am in page "Default"
    And I sign in
    Given I am in page "Dashboard"
    And I select project "NATP"
    And I am in page "Editor"
    When I click icon 'Update from Git' in panel 'Features'
    And I click on 'Select All' checkbox on pop-up "UPDATE DATA FROM GIT"
    And I click on button "Update" on pop-up "UPDATE DATA FROM GIT"
    Then I see success notification ""

    #gherkin specification
    When I select feature "search_book" in the tree
    And I expand scenario "Search books by publication year"
    Then I see that scenario description field having label 'Description' is displayed in Scenario Editor
    And I see that tags input control having label 'Tags' is displayed in Scenario Editor
    And I see that scenario steps editor having label 'Steps' is displayed in Scenario Editor
    And I see that scenario name and scenario description are not required to fill in Scenario Editor
    And I can edit scenario name and scenario description in Scenario Editor

    #background is displayed separately
    When I select a feature having a background in the tree
    Then I see that background is displayed in separated accordion item in Scenario Editor
    And I see that background has label 'BG' instead if index number in Scenario Editor
    And I see that scenarios have label 'Scenarios' above the first accordion in Scenario Editor

    #label 'Scenarios' should not be displayed when no scenarios present in the feature
    When I select feature "For_REL_840" in the tree
    And I select "123" scenario in the table
    And I click option "Delete" under drop-down 'Actions' in scenario accordion
    Then I see that label 'Scenarios' is not displayed in Scenario Editor
    When I select a feature in the tree having no scenarios
    Then I see that label 'Scenarios' is not displayed in Scenario Editor