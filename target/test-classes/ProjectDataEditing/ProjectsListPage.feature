@ProjectsListPage
Feature: Projects list page

  @REL-609
  Scenario:  Check pagination panel for projects on Dashboard page
    Given I am in page "Default"
    And I am signed in
    Given I am in page "Dashboard"
    And I have more than "10" accounts
    When I click on button "2" on pagination panel
    Then I am on the "2" page on pagination panel
    When I click on button "1" on pagination panel
    Then I am on the "1" page on pagination panel
    When I click on button ">" on pagination panel
    Then I am on the "2" page on pagination panel
    When I click on button "<" on pagination panel
    Then I am on the "1" page on pagination panel